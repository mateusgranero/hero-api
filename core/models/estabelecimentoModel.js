var mysql = require('mysql');
conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'hero'
});

module.exports = {
    gravar: gravar,
    editar: editar,
    login: login,
    selecionarEstabelecimentos: selecionarEstabelecimentos,
    selecionarEstabelecimentosPorId: selecionarEstabelecimentosPorId,
    selecionarEstabelecimentosPorIdSegmento: selecionarEstabelecimentosPorIdSegmento,
    gravarGoogle:gravarGoogle
};


function gravar(estabelecimento, callback) {
    var query = "insert into estabelecimento (NomeFantasia, RazaoSocial, Cnpj, Email, Senha, Segmento, Endereco,Lat_Google,Long_Google) "
    query += " values('" + estabelecimento.NomeFantasia + "','" + estabelecimento.RazaoSocial + "','" + estabelecimento.Cnpj + "','" + estabelecimento.Email + "','" + estabelecimento.Senha + "' ,'" + estabelecimento.Segmento + "',"
    query += "'" + estabelecimento.Endereco + "', " + "'" + estabelecimento.Lat_Google + "'" + "'" + estabelecimento.LongGoogle + "')";
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao inserir estabelecimento' });
        }
        else {
            callback(null, 'estabelecimento Inserido com sucesso!');
        }
    });
}

function gravarGoogle(estabelecimento, callback) {
    var query = "insert into estabelecimento (NomeFantasia, RazaoSocial, Cnpj, Email, Senha, Segmento, Endereco,Lat_Google,Long_Google) "
    query += " values('" + estabelecimento.name + "','" + estabelecimento.name + "',null,'null',null ,'" + estabelecimento.types[0] + "',"
    query += "'" + estabelecimento.vicinity + "', " + "'" + estabelecimento.geometry.location.lat + "','" + estabelecimento.geometry.location.lng + "')";
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao inserir estabelecimento' });
        }
        else {
            callback(null, 'estabelecimento Inserido com sucesso!');
        }
    });
}

function editar(estabelecimento, callback) {
    var query = "update estabelecimento set nome = '" + estabelecimento.nome + "', email = '" + estabelecimento.email + "',cpf = '" + estabelecimento.cpf + "' where Idestabelecimento = " + estabelecimento.Idestabelecimento;
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao editar estabelecimento' });
        }
        else {
            callback(null, 'estabelecimento alterado com sucesso!');
        }
    });
}

function login(login, senha, callback) {
    conn.query('SELECT IdEstabelecimento, RazaoSocial, NomeFantasia FROM estabelecimento WHERE email  ="' + login + '" and senha ="' + senha + '"', function (err, result, fields) {
        // conn.query('SELECT IdEstabelecimento, RazaoSocial, NomeFantasia FROM estabelecimento', function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            if (result.length > 0)
                callback(null, result[0]);
            else
                callback(null, null);
        }
    });
}

function selecionarEstabelecimentos(callback) {
    var query = 'select x.*,@curRank := @curRank + 1 AS rank from ('
    query += 'Select e.IdEstabelecimento,e.NomeFantasia,e.RazaoSocial,e.Email,e.Endereco, sum(coalesce(a.Resposta,0))/count(e.IdEstabelecimento) as Nota ,e.Lat_Google , e.Long_Google'
    query += ' from estabelecimento e '
    query += ' left join avaliacao a on a.IdEstabelecimento = e.IdEstabelecimento '
    query += ' group by e.IdEstabelecimento '
    query += ' order by Nota desc) as x cross join (SELECT @curRank := 0) r '
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            callback(null, result);
        }
    });
}

function selecionarEstabelecimentosPorId(idEstabelecimento, callback) {
    var query = 'Select e.IdEstabelecimento,e.NomeFantasia,e.RazaoSocial,e.Email,e.Endereco, sum(coalesce(a.Resposta,0))/count(e.IdEstabelecimento)  as Nota '
    query += 'from estabelecimento e '
    query += 'inner join avaliacao a on a.IdEstabelecimento = e.IdEstabelecimento '
    query += 'Where a.IdEstabelecimento = ' + idEstabelecimento;

    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            callback(null, result[0]);
        }
    });
}

function selecionarEstabelecimentosPorIdSegmento(idSegmento, callback) {
    var query = 'Select e.IdEstabelecimento,e.NomeFantasia,e.RazaoSocial,e.Email,e.Endereco, sum(coalesce(a.Resposta,0))/count(e.IdEstabelecimento)  as Nota '
    query += 'from estabelecimento e '
    query += 'inner join avaliacao a on a.IdEstabelecimento = e.IdEstabelecimento '
    query += 'Where a.idSegmento = ' + idSegmento;

    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            callback(null, result);
        }
    });
}