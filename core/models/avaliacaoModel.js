var mysql = require('mysql');
conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'hero'
});

module.exports = {
    gravar
}

function gravar(avaliacao, callback) {

    var query = 'insert into avaliacao (IdUsuario,IdPergunta,IdEstabelecimento,Resposta,DataHora) Values (' + avaliacao.IdUsuario + ',' + avaliacao.IdPergunta + ',' + avaliacao.IdEstabelecimento + ',' + avaliacao.Resposta + ',now())';

    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao Gravar avaliação' });
        }
        else {
            callback(null, result);
        }
    });

}

