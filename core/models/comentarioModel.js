var mysql = require('mysql');

module.exports = {
    gravar: gravar,
    selecionarComentarioPorEmpresa: selecionarComentarioPorEmpresa,
    selecionarComentarioPorIdPai: selecionarComentarioPorIdPai,
    selecionarTodosComentario: selecionarTodosComentario
};


function gravar(comentario, callback) {
    var  query = " insert into comentario (IdComentarioPai,IdUsuario,IdEmpresa,DataHora,Comentario,Status) values ("
    query += comentario.IdComentarioPai ? comentario.IdComentarioPai : null
    query += ","
    query += comentario.IdUsuario ? comentario.IdUsuario : null
    query += "," + comentario.IdEstabelecimento + ",now()," + "'" + comentario.Comentario + "'" + ",0);"
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, 'Erro ao inserir comentario');
        }
        else {
            callback(null, 'Comentario Inserido com sucesso!');
        }
    });
}

function selecionarComentarioPorEmpresa(idEstabelecimento, callback) {
    var query = 'Select c.*,u.Nome from comentario c '
    query += ' left join usuario u on c.IdUsuario = u.IdUsuario '
    query += ' Where IdEmpresa = ' + idEstabelecimento;
    query += ' order by DataHora desc'
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            callback(null, result);
        }
    });
}

function selecionarTodosComentario(callback) {
    var query = 'Select c.*, u.Nome from comentario c '
    query += ' left join usuario u on c.IdUsuario = u.IdUsuario '
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            callback(null, result);
        }
    });
}


function selecionarComentarioPorIdPai(idComentarioPai, callback) {
    var query = 'Select c.*, u.nome from comentario c '
    query += ' left join usuario u on c.IdUsuario = u.IdUsuario '
    query += ' Where IdComentarioPai = ' + idComentarioPai;
    conn.query(query, function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar estabelecimento' });
        }
        else {
            callback(null, result);
        }
    });
}
