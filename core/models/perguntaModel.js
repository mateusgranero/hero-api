var mysql = require('mysql');
conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'hero'
});

module.exports = {
    selecionarPerguntas:selecionarPerguntas
};

function selecionarPerguntas(callback) {
    conn.query('SELECT * FROM pergunta', function (err, result, fields) {
        if (err) {
            callback(500, { 'erro': 'Erro ao buscar perguntas' });
        }
        else {
            callback(null, result);
        }
    });
}