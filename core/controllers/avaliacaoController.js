var avaliacao = require('../models/avaliacaoModel.js');

module.exports = {
    gravar: gravar
};

function gravar(req, res, next) {
    avaliacao.gravar(req.body, function (err, data) {
        console.log(req.body);
        if (!err)
            res.status(200).json(data);
        else
            res.status(err).json(data);

    });
}

