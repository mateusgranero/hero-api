var usuario = require('../models/usuarioModel.js');

module.exports = {
    gravar: gravar,
    editar: editar,
    login: login
};

function gravar(req, res, next) {
    usuario.gravar(req.body, function (err, data) {
        if (err)
            res.status(err).json(data);
        else
            res.status(200).end();
    });
}

function editar(req, res, next) {
    usuario.editar(req.body, function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
        next();
    });
}

function login(req, res, next) {
    usuario.login(req.headers.login, req.headers.senha, function (err, data) {
        if (err)
            res.status(err).json(data);
        else {
            if (data)
                res.status(200).json(data);
            else
                res.status(204).end();
        }
        next();
    });
}


