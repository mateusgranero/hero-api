var estabelecimento = require('../models/estabelecimentoModel.js'),
    request = require('request'),
    waterfall = require('async-waterfall'),
    Promise = require('promise');;

module.exports = {
    gravar: gravar,
    editar: editar,
    login: login,
    selecionarEstabelecimentos: selecionarEstabelecimentos,
    selecionarEstabelecimentosPorId: selecionarEstabelecimentosPorId,
    selecionarEstabelecimentosPorIdSegmento: selecionarEstabelecimentosPorIdSegmento,
    cargaDeEstabelecimentos: cargaDeEstabelecimentos
};

function gravar(req, res, next) {
    estabelecimento.gravar(req.body, function (err, data) {
        if (!err) {
            res.status(200).json(data);
            next();
        } else {
            res.status(err).json(data);
            next();
        }
    });
}

function editar(req, res, next) {
    estabelecimento.editar(req.body, function (err, data) {
        if (!err) {
            res.status(200).json(req.body);
            next();
        } else {
            res.status(err).json(data);
            next();
        }
    });
}

function buscar(req, res, next) {
    estabelecimento.buscar(req.body, function (err, data) {
        if (!err) {
            res.status(200).json(data);
            next();
        } else {
            res.status(err).json(data);
            next();
        }
    });
}

function login(req, res, next) {
    estabelecimento.login(req.headers.login, req.headers.senha, function (err, data) {
        if (!err) {
            res.status(200).json(data);
            next();
        } else {
            res.status(err).json(data);
            next();
        }
    });
}

function selecionarEstabelecimentos(req, res, next) {
    estabelecimento.selecionarEstabelecimentos(function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
        next();
    });
}

function selecionarEstabelecimentosPorId(req, res, next) {
    estabelecimento.selecionarEstabelecimentosPorId(req.params.idEstabelecimento, function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
        next();
    });
}

function selecionarEstabelecimentosPorIdSegmento(req, res, next) {
    estabelecimento.selecionarEstabelecimentosPorIdSegmento(req.params.idSegmento, function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
        next();
    });
}

function cargaDeEstabelecimentos(req, res, next) {

    var options = {
        url: 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyC87NA5YJSikWZtFJL7-kcPV7mmRNFRDq4&location=-20.520113,-47.3838419&rankby=distance&type=airport|bakery|bank|cafe|food',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    request(options, function (err, response, data) {
        if (err)
            res.status(400).json(err)
        else {
            let promises = JSON.parse(data).results.map(x => new Promise((resolve, reject) => {
                estabelecimento.gravarGoogle(x, (err, data) => {
                    if (err)
                        reject(data);
                    else {
                        resolve();
                    }
                }, conn);
            }));

            Promise.all(promises).then(() => {
                res.status(200).end();
            }, (err) => {
                res.status(500).json(err);
            });

        }

    });

}


