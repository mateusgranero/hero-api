var pergunta = require('../models/perguntaModel.js');

module.exports = {
    selecionarPerguntas: selecionarPerguntas
};

function selecionarPerguntas(req, res, next) {
    pergunta.selecionarPerguntas(function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
        next();
    });
}
