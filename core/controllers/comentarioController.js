var comentario = require('../models/comentarioModel.js');

module.exports = {
    gravar: gravar,
    editar: editar,
    selecionarComentarioPorEmpresa: selecionarComentarioPorEmpresa,
    selecionarComentarioPorIdPai: selecionarComentarioPorIdPai,
    selecionarTodosComentario: selecionarTodosComentario
};

function gravar(req, res, next) {
    comentario.gravar(req.body, function (err, data) {
        if (!err)
            res.status(200).json(data);
        else
            res.status(err).json(data);
    });
}

function editar(req, res, next) {
    comentario.editar(req.body, function (err, data) {
        if (!err) {
            res.status(200).json(data);
        }
        else {
            res.status(err).json(data);
        }
    });
}

function selecionarComentarioPorEmpresa(req, res, next) {
    comentario.selecionarComentarioPorEmpresa(req.params.idEstabelecimento, function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
    });
}


function selecionarComentarioPorIdPai(req, res, next) {
    comentario.selecionarComentarioPorIdPai(req.params.idPai, function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
    });
}

function selecionarTodosComentario(req, res, next) {
    comentario.selecionarTodosComentario(function (err, data) {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(err).json(data);
        }
    });
}