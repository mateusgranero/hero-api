module.exports = function (app) {
    var comentario = app.core.controllers.comentarioController;

    app.route('/api/comentario')
        .post(comentario.gravar)

    app.route('/api/comentario/estabelecimento/:idEstabelecimento')
        .get(comentario.selecionarComentarioPorEmpresa)

    app.route('/api/comentario/pai/:idPai')
        .get(comentario.selecionarComentarioPorIdPai)

    app.route('/api/todosComentarios')
        .get(comentario.selecionarTodosComentario)

};