module.exports = function (app) {
    var pergunta = app.core.controllers.perguntaController;

    app.route('/api/pergunta')
        .get(pergunta.selecionarPerguntas)
};