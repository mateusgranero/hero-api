module.exports = function (app) {
    var estabelecimento = app.core.controllers.estabelecimentoController;
    
    app.route('/api/estabelecimento/login')
        .get(estabelecimento.login);

    app.route('/api/estabelecimento/:idEstabelecimento')
        .get(estabelecimento.selecionarEstabelecimentosPorId);

    app.route('/api/estabelecimento/segmento/:idSegmento')
        .get(estabelecimento.selecionarEstabelecimentosPorIdSegmento)
    
    app.route('/api/estabelecimento')
        .get(estabelecimento.selecionarEstabelecimentos)
        .post(estabelecimento.gravar)
        .put(estabelecimento.editar);

    app.route('/api/estabelecimento/carga')
        .post(estabelecimento.cargaDeEstabelecimentos)
};