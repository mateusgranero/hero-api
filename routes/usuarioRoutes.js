module.exports = function (app) {
    var usuario = app.core.controllers.usuarioController;
    
    app.route('/api/login')
        .get(usuario.login);
    
    app.route('/api/usuario')
        .post(usuario.gravar)
        .put(usuario.editar);
        
};