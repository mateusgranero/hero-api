var app = (require('express'))(),
    cors = require('cors'),
    cluster = require('cluster'),
    trace = require('./config/middleware/trace.js');
var contCluster = require('os').cpus().length;

if (!cluster.isMaster) {
    
    for (var i = 0; i < contCluster; i++) {
        cluster.fork();
    }

    cluster.on('exit', function(worker) {
        console.log('Worker %d died :(', worker.id);
        cluster.fork();
    });
}
else {
        (function loadJsonBodyParser() {
            var bodyParser = require('body-parser');
            app.use(bodyParser());
            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded());
        })();

        (function loadExpressValidator() {
            var expressValidator = require('express-validator');
            app.use(expressValidator());
        })();

        (function loadMiddleware() {
            var token = require('./config/middleware/token');
            app.use(token.validate);
        })();

        (function loadModelViewControllers() {
            app.use(cors());
            require('express-load')('core/ping/pingController.js')
                .then('core/models/usuarioModel.js')
                .then('core/controllers/usuarioController.js')
                .then('core/controllers/estabelecimentoController.js')
                .then('core/controllers/perguntaController.js')
                .then('core/controllers/avaliacaoController.js')
                .then('core/controllers/comentarioController.js')
                .then('routes')
                .into(app);
        })();

        (function loadMiddlewareErros() {
            var erros = require('./config/middleware/errors');
            app.use(erros.notfound);
            app.use(erros.serverError);
        })();

        app.listen(40001, function() {
            console.log('Express server listening on port ' + 40001);
        });
}