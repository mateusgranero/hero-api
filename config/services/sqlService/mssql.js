var sql = require('mssql');
var connectionPool = require('./mssql-connection');
var transaction = require('./mssql-transaction'),
    globalSettings = require('../../environment/globalSettings.js'),
    config = globalSettings.get().SqlServer;

function initialize(){
    var databaseConnection = connectionPool(config);
    
    return {
        connection: databaseConnection,
        transaction: () => { return transaction(databaseConnection); },
        request: (conn) => {return (conn instanceof sql.Transaction ? conn.request() : new sql.Request(conn || databaseConnection)) }
    }
}

module.exports = initialize();