var config = require('../../environment/globalSettings.js').get(),
    aion = require('amqplib'),
    waterfall = require('async-waterfall');

module.exports =  function (queueName){
    return {
        publisher: publisher(queueName),
        listener: listener(queueName)
    }
}

function publisher(queueName){  
    return function(message, onMessageSended){
        initialize(queueName, function(conn, channel){
            channel.sendToQueue(queueName, new Buffer(JSON.stringify(message)));
            onMessageSended && onMessageSended();
        });
    }
}

function listener(queueName){  
    return function(onMessageReceived){
        initialize(queueName, function(conn, channel){
            channel.consume(queueName, function(message) {
                if (message !== null){
                    onMessageReceived(JSON.parse(message.content), function(){
                        channel.ack(message);
                    })
                }
                else
                    channel.ack(message);
            });
        });
    }
}

function initialize(queueName, callback){
    waterfall([
            connectToQueue(queueName),
            createChanell,
            assertQueue
        ], 
        function(err, conn, channel){
            if(err){
                //log("QueueConnectionError", err)
                console.log('QueueConnectionError', err);
            }
            else{
                callback(conn, channel);
            }
        });
}

function connectToQueue(queueName){
    return function(callback){
        var connectionOk = aion.connect(config.Aion.uri)
        connectionOk.then(
            function(conn){
                callback(null, queueName, conn)
            },
            function(err){
                callback({
                        meta:{
                            date: new Date(),
                            queueName: queueName,
                            caller: 'infinitoOnline_api',
                            errorInfo:{
                                developerInfo: 'Ocorreu um erro ao abrir a conexão com a fila',
                                functionName: 'connectToQueue',
                                fileName: 'aion.js'
                            }
                        },
                        body: err
                    });
            });
    }
}

function createChanell(queueName, conn, callback){
    var queueChanell = conn.createChannel();
    queueChanell.then(
        function(channel){
            callback(null, queueName, conn, channel);
        },
        function(err){
            callback({
                    meta:{
                        date: new Date(),
                        queueName: queueName,
                        caller: 'infinitoOnline_api',
                        errorInfo:{
                            developerInfo: 'Ocorreu um erro ao abrir o canal com a fila',
                            functionName: 'createChanell',
                            fileName: 'aion.js'
                        }
                    },
                    body: err
                });
        });
}

function assertQueue(queueName, conn, channel, callback){
    channelOk = channel.assertQueue(queueName);
    channelOk.then(function(){
        callback(null, conn, channel);    
    },
    function(err){
        callback({
                meta:{
                    date: new Date(),
                    queueName: queueName,
                    caller: 'infinitoOnline_api',
                    errorInfo:{
                        developerInfo: 'Ocorreu um erro ao fazer assert da fila',
                        functionName: 'assertQueue',
                        fileName: 'aion.js'
                    }
                },
                body: err
            });
    })
}