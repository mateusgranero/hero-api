var request = require('request'),
    config = require('../../environment/globalSettings.js').get();

module.exports = {
    crypto: criptografar,
    uncrypto: descriptografar,
    encodeToken: encodeToken
}

function descriptografar(token, callback) {
    var options = {
        url: config.ApiAutenticacao.server + config.ApiAutenticacao.port + '/api/autenticacao/uncrypt',
        method: 'GET',
        headers: {
            'token': token
        }
    };
    request(options, (err, response, data) => {
        if (err) {
            callback(err, data);
        } else {
            var obj = JSON.parse(data);

            if (obj.statusCode) {
                callback(400, obj);
            } else {
                callback(null, obj.token);
            }
        }
    });
}

function criptografar(objeto, callback) {

    var options = {
        url: config.ApiAutenticacao.server + config.ApiAutenticacao.port + '/api/autenticacao/crypt',
        method: 'GET',
        headers: {
            'token': JSON.stringify(objeto)
        }
    };
    request(options, (err, response, data) => {
        if (err) {
            callback(err, data);
        } else {
            var obj = JSON.parse(data);

            if (obj.statusCode) {
                callback(400, obj);
            } else {
                callback(null, obj.token);
            }
        }
    });
}

function encodeToken(token) {
    return encodeURIComponent(token);
}