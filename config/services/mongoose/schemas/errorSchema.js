var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var logQueueConnection = new Schema({
    meta: {},
    body: {}
});

var logEmailSendErro = new Schema({
    meta:{},
    body:{}
});

mongoose.model('QueueConnectionError', logQueueConnection);
mongoose.model('EmailSendError', logEmailSendErro);