var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var traceSchema = new Schema({
    url: String,
    query: {},
    params: {},
    body: {},
    method: String,
    statusCode: Number,
    inicio: Date,
    fim: Date,
    error: {}
})

mongoose.model('trace', traceSchema);