require('./schemas/traceSchema.js');

var mongoose = require('mongoose'),
    config = require('../../environment/globalSettings.js').get(),
    mongoDbPath = config.MongoDb.server;

mongoose.connect(mongoDbPath, {server:{auto_reconnect:true}});
var db = mongoose.connection;

db.on('error', function (err) {
    console.error('MongoDB connection error:', err);
});
db.once('open', function callback() {
    console.info('MongoDB connection is established');
});
db.on('disconnected', function() {
    console.error('MongoDB disconnected!');
    mongoose.connect(mongoDbPath, {server:{auto_reconnect:true}});
});
db.on('reconnected', function () {
    console.info('MongoDB reconnected!');
});