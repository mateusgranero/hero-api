var mongoose = require('mongoose');

function saveUpdate(collectionName, registry){
    var schema = mongoose.model(collectionName);
    var registrySchema = new schema(registry);

    registrySchema.save(function(err) {
        if(err)
            console.log(err);
    });
}

module.exports = saveUpdate;