var mongoose = require('mongoose');

function saveError(collectionName, error){
    var schema = mongoose.model(collectionName);
    var errorSchema = new schema(error);

    errorSchema.save(function(err) {
        if(err)
            console.log(err);
    });
}

module.exports = saveError;