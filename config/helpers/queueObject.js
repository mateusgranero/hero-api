var config = require('../environment/globalSettings.js').get();

module.exports = {
    emailConfirmacaoPreCliente:function(preCliente, cryptToken) {
        return new queueEmail(preCliente,cryptToken);
    }
};

function queueEmail(preCliente,cryptToken) {
    this.meta = {
        source:"emailConfirmacaoPreCliente",
        countTentativas:0
    },
    this.body = {
        from: `Infinito Online <${config.ConfigEmail.auth.user}>`,
        to: `${preCliente.Nome} <${preCliente.Email}>`,
        subject: 'Confirmação de email',
        html: `<h1>Obrigado ${preCliente.Nome}</h1>, 
                <h3>por se cadastrar no Infinito Online</h3><br/>
                <p><a href="${config.ApiAdministracaoPortal.server}${config.ApiAdministracaoPortal.port}/api/precliente/confirmarCadastro/${ encodeURIComponent(cryptToken) }">
                Clique aqui para confirmação do seu cadastro</a></p>`
    }
}