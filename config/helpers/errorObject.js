module.exports = function (status, developerMessage, userMessage) {
    this.status = status;
    this.developerMessage = developerMessage;
    this.userMessage = [];
    this.addErros = adicionarErros;
    this.isValid = function () {
        return this.userMessage.length <= 0;
    };
    
    if(userMessage)
        this.addErros(userMessage);
        
    function adicionarErros(error) {
        this.userMessage.push(error);
    }
};