module.exports = function() {
    var String = global.String;
    var Number = global.Number;

    String.prototype.validarEmail = function() {
        return /^([\w\d]+[\.\-\_]?[\w\d]{1})+\@[\w\d]+(\.[\w]{2,3}){1,2}$/.test(this);
    };

    String.prototype.validarPassword = function() {
        return /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$/.test(this);
    };
};

// String.prototype.replaceAll = function(find, replace) {
//     return this.replace(new RegExp('\\' + find, 'g'), replace);
// };

// String.prototype.padLeft = function(length, caracter) {
//     var str = this.toString();
//     if (!caracter) caracter = '0';

//     while (str.length < length)
//         str = caracter + str;

//     return str;
// };
// String.prototype.padRight = function(length, caracter) {
//     var string = this.toString();
//     if (!caracter) caracter = '0';

//     while (string.length < length)
//         string = string + caracter;

//     return string;
// };