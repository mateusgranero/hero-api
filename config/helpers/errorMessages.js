module.exports = function() {
    var String = global.String;

    String.prototype.campoObrigatorio = function() {
        return `Campo “${this}” é obrigatório.`;
    };
    
    String.prototype.maximoCaracteres = function(max) {
        return `Campo “${this}” deve ter o máximo de ${max} caracteres.`;
    };
    
    String.prototype.dadosInvalidos = function() {
        return `Campo “${this}” recebeu um tipo de dado inesperado.`;
    };
};