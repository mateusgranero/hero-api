exports.notfound = function(req,res,next){
    if(!res.finished)
        res.json({
            developerMessage: 'Recurso não encontrado',
            userMessage: 'A operação solicitada não foi encontrada',
            errorCode: 404,
            moreInfo: 'api/Help'
        });
};

exports.serverError = function(err, req, res, next) {
    if(!res.finished){
        res.json(500, {
            developerMessage: err.message || err,
            userMessage: 'Não foi possivel concluir a operação solicitada',
            errorCode: err.status || 500
        });
    } 
};