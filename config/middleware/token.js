var request = require('request');

exports.validate = function (req, res, next) {

    res.header('Content-Type', 'application/json; application/xml; charset=utf-8');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    
    if (req.method === 'OPTIONS')
        res.status(204).send();

    return next();
};