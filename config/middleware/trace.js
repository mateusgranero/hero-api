var log = require('../services/mongoose/log/trace.js');

exports.begin = function (req, res, next) {
    req.inicio = new Date();
    next();
}

exports.error = function (err, req, res, next) {
    log('trace', {
        url: req.url,
        query: req.query,
        params: req.params,
        body: req.body,
        method: req.method,
        statusCode: res.statusCode,
        inicio: req.inicio,
        fim: new Date(),
        error: err
    });
};

exports.success = function (req, res, next) {
    log('trace', {
        url: req.url,
        query: req.query,
        params: req.params,
        body: req.body,
        method: req.method,
        statusCode: res.statusCode,
        inicio: req.inicio,
        fim: new Date(),
    });
};