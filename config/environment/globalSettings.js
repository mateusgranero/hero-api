var request = require('request'),
    localSettings = require('./localSettings.js');

module.exports = {
    load: load,
};

function load(start) {
    var options = {
        url: process.env.PARAMETERSPATH || 'http://192.168.7.32:40000/api/parameters/apiconfig/7,2',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'authorization': parameterToken()
        }
    };
    request(options, (err, response, data) => {
        if (response.statusCode == 200) {
            var globalSettings = JSON.parse(data);
            Object.assign(globalSettings, localSettings);
            process.env.infinito = JSON.stringify(globalSettings);
            start(null);
        } else
            start(response.statusCode, JSON.parse(data));
    });
}

function parameterToken() {
    var date = new Date();
    var valor = parseInt(`${date.getFullYear()}${padLeft((date.getMonth()), 2)}${padLeft(date.getDate(), 2)}`);
    return ((valor * 7777777)).toString();
}

function padLeft(value, length, caracter) {
    var str = value.toString();
    if (!caracter) caracter = '0';

    while (str.length < length)
        str = caracter + str;

    return str;
}